'use strict'
// Template version: 1.1.3
// see http://vuejs-templates.github.io/webpack for documentation.

const EXTRA_ROUTES = {
  POST: {},
  PUT: {},
  DELETE: {},
  PATCH: {}
}

// User REST API
EXTRA_ROUTES['POST']['/mock/cms_admin/user_list.json'] = '/mock/cms_admin/user_create.json'
EXTRA_ROUTES['PATCH']['/mock/cms_admin/user_show.json'] = '/mock/cms_admin/user_edit.json'
EXTRA_ROUTES['DELETE']['/mock/cms_admin/user_show.json'] = '/mock/cms_admin/user_remove.json'
// Game REST API
EXTRA_ROUTES['POST']['/mock/cms_admin/game_list.json'] = '/mock/cms_admin/game_create.json'
EXTRA_ROUTES['PUT']['/mock/front_site/game.json'] = '/mock/cms_admin/game_edit.json'
EXTRA_ROUTES['DELETE']['/mock/front_site/game.json'] = '/mock/cms_admin/game_remove.json'

const path = require('path')

module.exports = {
  build: {
    env: require('./prod.env'),
    index: path.resolve(__dirname, '../dist/index.html'),
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    assetsPublicPath: './',
    productionSourceMap: true,
    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],
    // Run the build command with an extra argument to
    // View the bundle analyzer report after build finishes:
    // `npm run build --report`
    // Set to `true` or `false` to always turn it on or off
    bundleAnalyzerReport: process.env.npm_config_report
  },
  dev: {
    env: require('./dev.env'),
    port: process.env.PORT || 8080,
    autoOpenBrowser: true,
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {
      '/api': {
        target: 'http://rctshowcase01.dftech.tw',
        changeOrigin: true,
        pathRewrite: {
          '^/api': '/api'
        }
      },
      '/api2': {
        target: 'http://localhost:8080',
        changeOrigin: true,
        pathRewrite: {
          '^/api/category/featured/games': '/mock/front_site/featured.json',
          '^/api/category/newgame/games': '/mock/front_site/newgame.json',
          '^/api/category/jackpot/games': '/mock/front_site/jackpot.json',
          '^/api/category/livedealer/games': '/mock/front_site/livedealer.json',
          '^/api/category/tablegames/games': '/mock/front_site/tablegames.json',
          '^/api/category/classicslots/games': '/mock/front_site/classicslots.json',
          '^/api/category/allslots/games': '/mock/front_site/allslots.json',
          '^/api/category': '/mock/front_site/category.json',
          '^/api/category/[A-Za-z0-9_]+/sort': '/mock/cms_admin/game_sort.json',
          '^/api/gameHistory': '/mock/front_site/game_history.json',

          '^/api/auth$': '/mock/auth.json',
          '^/api/message$': '/mock/smtp.json',
          '^/api/image$': '/mock/cms_admin/game_image_upload.json',
          '^/api/users/[A-Za-z0-9_]+': '/mock/cms_admin/user_edit.json',
          '^/api/users': '/mock/cms_admin/user_list.json',
          '^/api/auditlogs/users/[A-Za-z0-9_]+': '/mock/cms_admin/user_audit_logs.json',

          '^/api/game/[A-Za-z0-9_]+/all': '/mock/cms_admin/game_detail.json',
          '^/api/game/[A-Za-z0-9_]+': '/mock/front_site/game.json',
          '^/api/game': '/mock/cms_admin/game_list.json',
          '^/api/game2': '/mock/cms_admin/category_newgame.json',
          '^/api/banners/all': '/mock/cms_admin/banner_list.json',
          '^/api/banners': '/mock/front_site/banner.json'
        },
        onProxyReq: (proxyReq, req, res) => {
          proxyReq.setHeader('Authorization', null)
          let methods = ['POST', 'DELETE', 'PUT', 'PATCH']
          if (methods.includes(req.method)) {
            proxyReq.path = EXTRA_ROUTES[req.method][proxyReq.path] || proxyReq.path
            proxyReq.method = 'GET'
          }
        }
      }
    },
    // CSS Sourcemaps off by default because relative paths are "buggy"
    // with this option, according to the CSS-Loader README
    // (https://github.com/webpack/css-loader#sourcemaps)
    // In our experience, they generally work as expected,
    // just be aware of this issue when enabling this option.
    cssSourceMap: false
  }
}
