import { validationMixin } from 'vuelidate'
import { required, minLength, maxLength, email, numeric } from 'vuelidate/lib/validators'

export default {
  mixins: [validationMixin],
  validations: {
    message: {
      firstname: {
        required,
        maxLength: maxLength(40)
      },
      lastname: {
        required,
        maxLength: maxLength(40)
      },
      email: {
        required,
        minLength: minLength(8),
        maxLength: maxLength(50),
        email
      },
      phone: {
        required,
        minLength: minLength(2),
        maxLength: maxLength(20),
        numeric
      },
      company: {
        required,
        minLength: minLength(2),
        maxLength: maxLength(30)
      },
      context: {
        required,
        minLength: minLength(2),
        maxLength: maxLength(1000)
      }
    }
  }
}
