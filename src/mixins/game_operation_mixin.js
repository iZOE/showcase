export default {
  data: function () {
    return {
      // Upload Image Parameter
      imageFiles: [],

      // Component Variables
      imageData: [],
      options: [],
      nextRoute: '',
      isAnyChange: false
    }
  },
  methods: {
    getCategories: async function () {
      const res = await this.actionCmsGetAllCategories()
      this.options = []
      res.filter((category) => {
        let option = {}
        option.text = category.Title
        option.value = category.Name
        this.options.push(option)
      })
    },
    applyGameFields: function (category, gameDetails) {
      this.selected = category
      if (gameDetails.length < this.gameLocales.length) {
        let diff = this.gameLocales.length - gameDetails.length
        for (let x = 0; x < diff; x++) {
          gameDetails.push({})
        }
      }
      gameDetails.forEach((detail, idx) => {
        this.displayNames[idx] = detail.Title
        this.gameLinks[idx] = detail.Link
        this.mobileLinks[idx] = detail.MobileLink
        this.descriptions[idx] = detail.Description
        this.imageLinks[idx] = detail.Thumbnail
        this.imageData[idx] = detail.Thumbnail
        if (this.$refs[`imageFiles[${idx}]`]) {
          this.$refs[`imageFiles[${idx}]`].reset()
        }
      })
      this.$forceUpdate()
    },
    previewImage: function (evt, idx) {
      this.confirmChanges()
      let files = evt.target.files || evt.dataTransfer.files
      let file = files[0] || window.app.file
      idx = idx || 0
      if (file && this.verifyImageFile(idx, file)) {
        let reader = new FileReader()
        reader.onload = (e) => {
          this.uploadGameImage(idx, file)
          this.imageData[idx] = e.target.result
          this.$forceUpdate()
        }
        reader.readAsDataURL(file)
      } else {
        this.imageData[idx] = ''
        this.imageLinks[idx] = ''
        this.$forceUpdate()
      }
    },
    uploadGameImage: async function (idx, file) {
      let res = await this.actionCmsUploadGameImage({
        token: this.userToken,
        file: file
      })
      this.imageLinks[idx] = res.Path
      this.verifyImageLink(idx)
    },
    removePreviewImg: function (idx) {
      this.imageFiles[idx] = ''
      this.imageLinks[idx] = ''
      this.imageData[idx] = ''
      this.$forceUpdate()
    },
    buildGameParams: function () {
      let details = []
      this.gameLocales.forEach((locale, idx) => {
        // Skip Non-Default Empty Locale
        if (idx > 0 && (this.displayNames[idx] == null || this.displayNames[idx].length === 0)) {
          return
        }

        let gameDetails = {
          locale: locale,
          title: this.displayNames[idx],
          description: this.descriptions[idx],
          main_image: this.imageLinks[idx],
          thumbnail: this.imageLinks[idx],
          link: this.gameLinks[idx],
          mobileLink: this.mobileLinks[idx]
        }
        details.push(gameDetails)
      })

      let game = {
        name: this.gameName,
        category: this.selected,
        details: details
      }

      return game
    },
    hasChanges: function () {
      return this.isAnyChange
    },
    resetChanges: function () {
      this.isAnyChange = false
    },
    confirmChanges: function () {
      this.isAnyChange = true
    },
    redirectPage: function (path = this.nextRoute) {
      this.resetChanges()
      this.$router.push(path)
    }
  },
  beforeRouteLeave: function (to, from, next) {
    if (!this.hasChanges() || to.name === 'logout') {
      next()
    } else {
      this.nextRoute = to.fullPath
      this.$refs.leavePage.leaveConfirm()
      next(false)
    }
  }
}
