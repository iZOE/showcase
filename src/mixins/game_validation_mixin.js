const URL_PATTERN = /^(http|https):\/\/.*$/
const IMAGE_PATTERN = /\.(jpe?g|png)$/i
const IMAGE_TYPE_PATTERN = /image\/(jpe?g|png)$/i
const IMAGE_MAX_SIZE = 512 * 1024
const GAME_ID_PATTERN = /^[a-zA-Z0-9._]{1,128}$/

export default {
  data: function () {
    return {
      valid: {
        gameName: null,
        detail: [{}]
      }
    }
  },
  methods: {
    isCreateValid: function () {
      return this.isEverythingValid(true)
    },
    isEditValid: function () {
      return this.isEverythingValid()
    },
    isEverythingValid: function (isCreate = false) {
      let valid = true
      if (isCreate) {
        this.verifyGameName()
      }

      this.gameLocales.forEach((locale, idx) => {
        if (this.isLocaleEmpty(idx)) {
          return
        }

        this.verifyDisplayName(idx)
        this.verifyGameLink(idx)
        this.verifyImageLink(idx)
        this.verifyDescription(idx)

        valid = Boolean(valid && this.valid['detail'][idx]['displayName'])
        valid = Boolean(valid && this.valid['detail'][idx]['gameLink'])
        valid = Boolean(valid && this.valid['detail'][idx]['imageFile'])
        valid = Boolean(valid && this.valid['detail'][idx]['description'])
      })

      if (valid !== true) {
        console.log('Validation Fail')
        this.$forceUpdate()
      }

      return valid
    },
    isLocaleEmpty: function (idx) {
      if (idx < 1) {
        return false
      }

      let isLocaleEmpty = true

      isLocaleEmpty = Boolean(isLocaleEmpty && ((this.displayNames[idx] == null) || (this.displayNames[idx].length === 0)))
      isLocaleEmpty = Boolean(isLocaleEmpty && ((this.gameLinks[idx] == null) || (this.gameLinks[idx].length === 0)))
      isLocaleEmpty = Boolean(isLocaleEmpty && ((this.mobileLinks[idx] == null) || (this.mobileLinks[idx].length === 0)))
      isLocaleEmpty = Boolean(isLocaleEmpty && ((this.descriptions[idx] == null) || (this.descriptions[idx].length === 0)))
      isLocaleEmpty = Boolean(isLocaleEmpty && ((this.imageData[idx] == null) || (this.imageData[idx].length === 0)))

      this.clearAllValidations(idx)

      return isLocaleEmpty
    },
    clearAllValidations: function (idx) {
      this.valid['detail'][idx]['displayName'] = null
      this.valid['detail'][idx]['gameLink'] = null
      this.valid['detail'][idx]['mobileLink'] = null
      this.valid['detail'][idx]['imageFile'] = null
      this.valid['detail'][idx]['description'] = null
    },
    verify: function (tier, idx = 0, key = '') {
      this.confirmChanges()
      switch (tier) {
        case 'gameName':
          this.verifyGameName()
          break
        case 'detail':
          this.verifyDetail(idx, key)
          break
        default:
          break
      }
      this.$forceUpdate()
    },
    verifyDetail: function (idx, key) {
      switch (key) {
        case 'displayName':
          this.verifyDisplayName(idx)
          break
        case 'gameLink':
          this.verifyGameLink(idx)
          break
        case 'mobileLink':
          this.verifyMobileLink(idx)
          break
        case 'description':
          this.verifyDescription(idx)
          break
        default:
          break
      }
    },
    verifyGameName: async function () {
      if (GAME_ID_PATTERN.test(this.gameName)) {
        let payload = {
          token: this.userToken,
          gameName: this.gameName
        }
        try {
          await this.actionCmsIsGameNameExist(payload)
          this.gameNameDuplicated = true
          this.valid['gameName'] = false
        } catch (err) {
          console.warn('err', err)
          if (err.body.ReturnCode === '0x10780008') {
            this.valid['gameName'] = true
          }
          throw err
        }
      } else {
        this.valid['gameName'] = false
      }
    },
    verifyDisplayName: function (idx) {
      this.valid['detail'][idx]['displayName'] = Boolean(this.displayNames[idx] && this.displayNames[idx].length > 3)
    },
    verifyGameLink: function (idx) {
      this.valid['detail'][idx]['gameLink'] = Boolean(this.gameLinks[idx] && URL_PATTERN.test(this.gameLinks[idx]))
    },
    verifyMobileLink: function (idx) {
      let isEmpty = (this.mobileLinks[idx] == null) || (this.mobileLinks[idx].length === 0)
      this.valid['detail'][idx]['mobileLink'] = Boolean(isEmpty || URL_PATTERN.test(this.mobileLinks[idx]))
    },
    verifyImageLink: function (idx) {
      this.valid['detail'][idx]['imageFile'] = Boolean(this.imageLinks[idx] && IMAGE_PATTERN.test(this.imageLinks[idx]))
    },
    verifyDescription: function (idx) {
      let validDescription = Boolean((this.descriptions[idx] == null) || (this.descriptions[idx].length < 1000))
      this.valid['detail'][idx]['description'] = validDescription
    },
    verifyImageFile: function (idx, file) {
      let valid = Boolean(file && IMAGE_PATTERN.test(file.name) && IMAGE_TYPE_PATTERN.test(file.type) && (file.size < IMAGE_MAX_SIZE))
      this.valid['detail'][idx]['imageFile'] = valid
      return valid
    },
    hintGameName: function () {
      let hint = ''
      if (this.gameName.length === 0) {
        hint = 'Game ID is required field'
      } else if (this.gameName.length > 128) {
        hint = 'The text entered exceeds the maximum length'
      } else if (!GAME_ID_PATTERN.test(this.gameName)) {
        hint = 'Game ID must not contain special characters such as: "<>/%#&{}[]~"'
      } else if (this.gameNameDuplicated) {
        hint = 'Game ID Duplicated. Please use a different one.'
      } else {
        hint = 'Please input valid data'
      }
      return hint
    },
    hintDisplayName: function (idx) {
      let hint = 'Please input valid data'
      if (this.displayNames[idx] && this.displayNames[idx].length === 0) {
        hint = 'Display Name is required field'
      } else if (this.displayNames[idx] && this.displayNames[idx].length < 4) {
        hint = 'Enter at least 4 characters'
      } else if (this.displayNames[idx] && this.displayNames[idx].length > 128) {
        hint = 'The text entered exceeds the maximum length'
      } else {
        hint = 'Please input valid data'
      }
      return hint
    },
    hintGameLink: function (idx) {
      return (this.gameLinks[idx] && this.gameLinks[idx].length > 0) ? 'Please enter a valid Game Link' : 'Game Link is required field'
    },
    hintMobileLink: function (idx) {
      return 'Please enter a valid Mobile Link'
    },
    hintImageFile: function (idx) {
      return 'Please Insert Valid Game Image'
    },
    hintDescription: function (idx) {
      return 'Content Too Long'
    }
  }
}
