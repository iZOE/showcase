import MobileDetect from 'mobile-detect'

const mobileDetect = new MobileDetect(window.navigator.userAgent)

export default {
  computed: {
    isMobile: function () {
      return Boolean(mobileDetect.mobile())
    }
  }
}
