export default {
  data: function () {
    return {
      // Upload Image Parameter
      imageFiles: [],

      // Component Variables
      imageData: [],
      options: [],
      nextRoute: '',
      isAnyChange: false
    }
  },
  methods: {
    applyBannerFields: function (bannerDetails) {
      if (bannerDetails.length < this.bannerLocales.length) {
        let diff = this.bannerLocales.length - bannerDetails.length
        for (let x = 0; x < diff; x++) {
          bannerDetails.push({})
        }
      }
      bannerDetails.forEach((detail, idx) => {
        this.bannerTitles[idx] = detail.Title
        this.bannerLinks[idx] = detail.Link
        this.imageLinks[idx] = detail.Thumbnail
        this.imageData[idx] = detail.Thumbnail
        if (this.$refs[`imageFiles[${idx}]`]) {
          this.$refs[`imageFiles[${idx}]`].reset()
        }
      })
      this.$forceUpdate()
    },
    previewImage: function (evt, idx) {
      this.confirmChanges()
      let files = evt.target.files || evt.dataTransfer.files
      let file = files[0] || window.app.file
      idx = idx || 0
      if (file && this.verifyImageFile(idx, file)) {
        let reader = new FileReader()
        reader.onload = (e) => {
          this.uploadBannerImage(idx, file)
          this.imageData[idx] = e.target.result
          this.$forceUpdate()
        }
        reader.readAsDataURL(file)
      } else {
        this.imageData[idx] = ''
        this.imageLinks[idx] = ''
        this.$forceUpdate()
      }
    },
    uploadBannerImage: async function (idx, file) {
      let res = await this.actionCmsUploadBannerImage({
        token: this.userToken,
        file: file
      })
      this.imageLinks[idx] = res.Path
      this.verifyImageLink(idx)
    },
    removePreviewImg: function (idx) {
      this.imageFiles[idx] = ''
      this.imageLinks[idx] = ''
      this.imageData[idx] = ''
      this.$forceUpdate()
    },
    buildBannerParams: function () {
      let details = []
      this.bannerLocales.forEach((locale, idx) => {
        // Skip Non-Default Empty Locale
        if (idx > 0 && (this.bannerTitles[idx] == null || this.bannerTitles[idx].length === 0)) {
          return
        }

        let bannerDetails = {
          locale: locale,
          title: this.bannerTitles[idx],
          main_image: this.imageLinks[idx],
          thumbnail: this.imageLinks[idx],
          link: this.bannerLinks[idx]
        }
        details.push(bannerDetails)
      })

      let banner = {
        details: details
      }

      return banner
    },
    hasChanges: function () {
      return this.isAnyChange
    },
    resetChanges: function () {
      this.isAnyChange = false
    },
    confirmChanges: function () {
      this.isAnyChange = true
    },
    redirectPage: function (path = this.nextRoute) {
      this.resetChanges()
      this.$router.push(path)
    }
  },
  beforeRouteLeave: function (to, from, next) {
    if (!this.hasChanges() || to.name === 'logout') {
      next()
    } else {
      this.nextRoute = to.fullPath
      this.$refs.leavePage.leaveConfirm()
      next(false)
    }
  }
}
