const URL_PATTERN = /^(http|https):\/\/.*$/
const IMAGE_PATTERN = /\.(jpe?g|png)$/i
const IMAGE_TYPE_PATTERN = /image\/(jpe?g|png)$/i
const IMAGE_MAX_SIZE = 2048 * 1024
const BANNER_TITLE_PATTERN = /^.{1,128}$/

export default {
  data: function () {
    return {
      valid: {
        detail: [{}]
      }
    }
  },
  methods: {
    isCreateValid: function () {
      return this.isEverythingValid(true)
    },
    isEditValid: function () {
      return this.isEverythingValid()
    },
    isEverythingValid: function (isCreate = false) {
      let valid = true

      this.bannerLocales.forEach((locale, idx) => {
        if (this.isLocaleEmpty(idx)) {
          return
        }

        this.verifyBannerTitle(idx)
        this.verifyBannerLink(idx)
        this.verifyImageLink(idx)

        valid = Boolean(valid && this.valid['detail'][idx]['bannerTitle'])
        valid = Boolean(valid && this.valid['detail'][idx]['bannerLink'])
        valid = Boolean(valid && this.valid['detail'][idx]['imageFile'])
      })

      if (valid !== true) {
        console.log('Validation Fail')
        this.$forceUpdate()
      }

      return valid
    },
    isLocaleEmpty: function (idx) {
      if (idx < 1) {
        return false
      }

      let isLocaleEmpty = true

      isLocaleEmpty = Boolean(isLocaleEmpty && ((this.bannerTitles[idx] == null) || (this.bannerTitles[idx].length === 0)))
      isLocaleEmpty = Boolean(isLocaleEmpty && ((this.bannerLinks[idx] == null) || (this.bannerLinks[idx].length === 0)))
      isLocaleEmpty = Boolean(isLocaleEmpty && ((this.imageData[idx] == null) || (this.imageData[idx].length === 0)))

      this.clearAllValidations(idx)

      return isLocaleEmpty
    },
    clearAllValidations: function (idx) {
      this.valid['detail'][idx]['bannerTitle'] = null
      this.valid['detail'][idx]['bannerLink'] = null
      this.valid['detail'][idx]['imageFile'] = null
    },
    verify: function (tier, idx = 0, key = '') {
      this.confirmChanges()
      switch (tier) {
        case 'detail':
          this.verifyDetail(idx, key)
          break
        default:
          break
      }
      this.$forceUpdate()
    },
    verifyDetail: function (idx, key) {
      switch (key) {
        case 'bannerTitle':
          this.verifyBannerTitle(idx)
          break
        case 'bannerLink':
          this.verifyBannerLink(idx)
          break
        default:
          break
      }
    },
    verifyBannerTitle: function (idx) {
      this.valid['detail'][idx]['bannerTitle'] = Boolean(BANNER_TITLE_PATTERN.test(this.bannerTitles[idx]) && this.bannerTitles[idx] && this.bannerTitles[idx].length > 3)
    },
    verifyBannerLink: function (idx) {
      let isEmpty = (this.bannerLinks[idx] == null) || (this.bannerLinks[idx].length === 0)
      this.valid['detail'][idx]['bannerLink'] = Boolean(isEmpty || URL_PATTERN.test(this.bannerLinks[idx]))
    },
    verifyImageLink: function (idx) {
      this.valid['detail'][idx]['imageFile'] = Boolean(this.imageLinks[idx] && IMAGE_PATTERN.test(this.imageLinks[idx]))
    },
    verifyImageFile: function (idx, file) {
      let valid = Boolean(file && IMAGE_PATTERN.test(file.name) && IMAGE_TYPE_PATTERN.test(file.type) && (file.size < IMAGE_MAX_SIZE))
      this.valid['detail'][idx]['imageFile'] = valid
      return valid
    },
    hintBannerTitle: function (idx) {
      let hint = ''
      if (this.bannerTitles[idx] && this.bannerTitles[idx].length === 0) {
        hint = 'Banner Title is required field'
      } else if (this.bannerTitles[idx] && this.bannerTitles[idx].length < 4) {
        hint = 'Enter at least 4 characters'
      } else if (this.bannerTitles[idx] && this.bannerTitles[idx].length > 128) {
        hint = 'The text entered exceeds the maximum length'
      } else {
        hint = 'Please input valid data'
      }
      return hint
    },
    hintBannerLink: function (idx) {
      return 'Please enter a valid URL'
    },
    hintImageFile: function (idx) {
      return 'Please Insert Valid Banner Image'
    }
  }
}
