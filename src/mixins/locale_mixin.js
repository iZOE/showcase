export default {
  methods: {
    $tm: function (input) {
      return (/^#[a-zA-Z0-9._-]*#$/.test(input)) ? this.$t(input) : input
    },
    getLocaleTitle: function (locale) {
      if (locale === 'en-us') return 'EN'
      else if (locale === 'zh-cn') return 'CN'
      else return 'UNKNOWN'
    },
    getLocaleTitles: function (locales) {
      return locales.map(this.getLocaleTitle).join(' / ')
    }
  }
}
