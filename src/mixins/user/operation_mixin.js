const PASSWORD_LENGTH_MIN = 12
const PASSWORD_LENGTH_MAX = 20
const PASSWORD_PATTERN = /^[a-zA-Z0-9_.!@]{4,50}$/

export default {
  data: function () {
    return {
      nextRoute: '',
      autoPassword: '',
      isPasswordShown: false,
      isConfirmPasswordShown: false
    }
  },
  methods: {
    createUserParams: function () {
      let createPlayerDetails = {
        username: this.createPlayerDetails.username,
        firstname: this.createPlayerDetails.firstname,
        lastname: this.createPlayerDetails.lastname,
        email: this.createPlayerDetails.email,
        password: this.createPlayerDetails.password,
        phone: this.createPlayerDetails.phone,
        company: this.createPlayerDetails.company
      }

      return createPlayerDetails
    },
    updateUserParams: function () {
      let updatePlayerDetails = {
        Status: this.updatePlayerDetails.status,
        UserData: {
          FirstName: this.updatePlayerDetails.firstname,
          LastName: this.updatePlayerDetails.lastname,
          Company: this.updatePlayerDetails.company
        },
        Email: this.updatePlayerDetails.email,
        Phone: this.updatePlayerDetails.phone
      }

      return updatePlayerDetails
    },
    updateUserPasswordParam: function () {
      let updatePlayerPassword = {
        Password: this.updatePlayerPassword.password
      }

      return updatePlayerPassword
    },
    applyPlayerFields: function (userDetails) {
      this.updatePlayerDetails.username = userDetails.Username
      this.updatePlayerDetails.firstname = userDetails.UserData.FirstName
      this.updatePlayerDetails.lastname = userDetails.UserData.LastName
      this.updatePlayerDetails.status = userDetails.Status
      this.updatePlayerDetails.email = userDetails.Email
      this.updatePlayerDetails.phone = userDetails.Phone
      this.updatePlayerDetails.company = userDetails.UserData.Company
      this.updatePlayerDetails.mgUser = userDetails.MgUser
      this.updatePlayerDetails.password = userDetails.UserData.Password
    },
    redirectPage: function (path = this.nextRoute) {
      this.$v.$reset()
      this.$router.push(path)
    },
    generatePassword: function () {
      let randomLength = Math.floor(Math.random() * (PASSWORD_LENGTH_MAX - PASSWORD_LENGTH_MIN + 1)) + PASSWORD_LENGTH_MIN
      let password = ''

      while (!PASSWORD_PATTERN.test(password)) {
        password = Buffer.from(Math.random().toString(36)).toString('base64').substring(3, randomLength)
        this.applyPassword(password)
      }
    },
    togglePassword: function () {
      this.isPasswordShown = !this.isPasswordShown
    },
    toggleConfirmPassword: function () {
      this.isConfirmPasswordShown = !this.isConfirmPasswordShown
    },
    getFieldType: function (isPasswordShown) {
      return (isPasswordShown) ? 'text' : 'password'
    }
  },
  beforeRouteLeave: function (to, from, next) {
    if (!this.isFormChanged || to.name === 'logout') {
      next()
    } else {
      this.nextRoute = to.fullPath
      this.$refs.leavePage.leaveConfirm()
      next(false)
    }
  }
}
