import { validationMixin } from 'vuelidate'
import { required, minLength, maxLength, email, numeric, sameAs, helpers } from 'vuelidate/lib/validators'
import { USER_VALIDATION_PATTERN, USERNAME_ALL_IN_ONE_PATTERN } from '@/constants/validation_pattern'

const username = helpers.regex('username', USER_VALIDATION_PATTERN.USERNAME)
const firstname = helpers.regex('firstname', USER_VALIDATION_PATTERN.FIRSTNAME)
const lastname = helpers.regex('lastname', USER_VALIDATION_PATTERN.LASTNAME)
const password = helpers.regex('password', USER_VALIDATION_PATTERN.PASSWORD)
const company = helpers.regex('company', USER_VALIDATION_PATTERN.COMPANY)

export default {
  mixins: [validationMixin],
  data: function () {
    return {
      nameDebounceEvent$: undefined
    }
  },
  validations: {
    createPlayerDetails: {
      username: {
        required,
        minLength: minLength(4),
        maxLength: maxLength(20),
        username,
        isUnique (value) {
          if (!USERNAME_ALL_IN_ONE_PATTERN.test(value)) return true

          return new Promise((resolve, reject) => {
            clearTimeout(this.nameDebounceEvent$)
            this.nameDebounceEvent$ = setTimeout(() => {
              resolve(this.checkUsernameExist(value))
            }, 600)
          })
        }
      },
      firstname: {
        required,
        minLength: minLength(2),
        maxLength: maxLength(50),
        firstname
      },
      lastname: {
        required,
        minLength: minLength(2),
        maxLength: maxLength(50),
        lastname
      },
      email: {
        required,
        minLength: minLength(8),
        maxLength: maxLength(50),
        email
      },
      password: {
        required,
        minLength: minLength(4),
        maxLength: maxLength(50),
        password
      },
      confirmPassword: {
        sameAsPassword: sameAs('password')
      },
      phone: {
        minLength: minLength(2),
        maxLength: maxLength(50),
        numeric
      },
      company: {
        minLength: minLength(2),
        maxLength: maxLength(50),
        company
      }
    },
    updatePlayerDetails: {
      firstname: {
        required,
        minLength: minLength(2),
        maxLength: maxLength(50),
        firstname
      },
      lastname: {
        required,
        minLength: minLength(2),
        maxLength: maxLength(50),
        lastname
      },
      status: {
        required
      },
      email: {
        required,
        minLength: minLength(8),
        maxLength: maxLength(50),
        email
      },
      phone: {
        minLength: minLength(2),
        maxLength: maxLength(50),
        numeric
      },
      company: {
        minLength: minLength(2),
        maxLength: maxLength(50),
        company
      }
    },
    updatePlayerPassword: {
      password: {
        required,
        minLength: minLength(4),
        maxLength: maxLength(50),
        password
      },
      confirmPassword: {
        sameAsPassword: sameAs('password')
      }
    }
  },
  methods: {
    checkUsernameExist: async function (username) {
      let payload = {
        token: this.userToken,
        username: username
      }

      try {
        await this.actionCmsIsUsernameExist(payload)
        return false
      } catch (err) {
        return true
      }
    }
  }
}
