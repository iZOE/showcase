const error = {
  '0x10770001_WrongParameter': '#Error.Msg.0x10770001.WrongParameter#',
  '0x10780001_AuthenticationFail': '#Error.Msg.0x10780001.AuthenticationFail#',
  '0x10780002_InvalidPermission': '#Error.Msg.0x10780002.InvalidPermission#'
}

const errMsg = '#Error.Msg.Default#' // for default error msg

export default {
  methods: {
    errorMapping: function (errCode, errType) {
      return error[errCode + '_' + errType] || errMsg
    }
  }
}
