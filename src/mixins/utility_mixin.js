export default {
  methods: {
    isDataEmpty: function (item) {
      return (item == null) || (item.length === 0)
    }
  }
}
