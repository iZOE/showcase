export const BEARER = 'Bearer'

export const BODY_TYPE_FORM_DATA = 'multipart/form-data'
export const BODY_TYPE_JSON = 'application/json'

export const DEFAULT_LOCALE = 'en-US'
