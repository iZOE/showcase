import Vue from 'vue'

/**
 * Authentication Service
 * that calls server side to get token
 */
export default {
  /**
   * Login API to get the token
   *
   * @param {object} user account and password
   */
  login: function (user) {
    return Vue.http.post('./api/auth', user)
  }
}
