import Vue from 'vue'

import {
  BEARER,
  BODY_TYPE_JSON,
  DEFAULT_LOCALE
} from '@/services/constants'

/**
 * Category Service
 * that retrieves category information
 */
export default {

  /** Methods Without Authentication **/

  /**
   * Get categories
   *
   * @param {*} locale
   */
  getCategories: function (locale = DEFAULT_LOCALE) {
    return Vue.http.get(`./api/category?loc=${locale}`)
  },

  /**
   * Get category games
   *
   * @param {*} locale
   * @param {*} isMobile
   * @param {*} category
   */
  getCategoryGames: function (category, locale = DEFAULT_LOCALE, isMobile = false) {
    let categoryUri = `./api/category/${category}/games?loc=${locale}`
    categoryUri += (isMobile) ? '&device=mobile' : ''
    return Vue.http.get(categoryUri)
  },

  /** Methods With Authentication **/

  /**
   * Sort category games
   *
   * @param {*} token
   * @param {*} category
   * @param {*} ordering
   */
  sortCategoryGames: function (token, category, ordering) {
    return Vue.http.put(`./api/category/${category}/sort`, ordering, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  }
}
