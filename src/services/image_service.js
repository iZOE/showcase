import Vue from 'vue'

import {
  BEARER,
  BODY_TYPE_FORM_DATA
} from '@/services/constants'

/**
 * Image Service
 * that uploads image to server
 */
export default {
  /**
   * Upload Image API, use FormData to send the request
   *
   * @param {*} token
   * @param {*} formData
   */
  uploadImage: function (token, formData) {
    return Vue.http.post('./api/image', formData, {
      'headers': {
        'Content-Type': BODY_TYPE_FORM_DATA,
        'Authorization': `${BEARER} ${token}`
      }
    })
  }
}
