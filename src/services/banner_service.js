import Vue from 'vue'

import {
  BEARER,
  BODY_TYPE_JSON
} from '@/services/constants'

/**
 * Banner Service
 * that request Banner Content from server side
 */
export default {

  /** Methods Without Authentication **/

  /**
   * Get server side banners for front site
   */
  getBanners: function (locale) {
    return Vue.http.get(`./api/banners?loc=${locale}`)
  },

  /** Methods With Authentication **/

  /**
   * Get server side banners for CMS
   */
  getAllBanners: function (token) {
    return Vue.http.get(`./api/banners/all`, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Sort banners
   */
  sortAllBanners: function (token, ordering) {
    return Vue.http.put(`./api/banners/sort`, ordering, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Add new banner
   */
  addBanner: function (token, banner) {
    return Vue.http.post(`./api/banner`, banner, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Delete banner by banner id
   */
  removeBanner: function (token, bannerId) {
    return Vue.http.delete(`./api/banner/${bannerId}`, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Get banner details by the banner ID
   */
  getBannerDetails: function (token, bannerId) {
    return Vue.http.get(`./api/banner/${bannerId}`, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Edit banner details by the banner Id
   */
  editBannerDetails: function (token, bannerId, bannerDetails) {
    return Vue.http.put(`./api/banner/${bannerId}`, bannerDetails, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  }
}
