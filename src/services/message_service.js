import Vue from 'vue'

/**
 * Messaging Service
 * that sends message to server
 */
export default {
  /**
   * Send Message API
   */
  sendMessage: function (message) {
    return Vue.http.post('./api/message', message)
  }
}
