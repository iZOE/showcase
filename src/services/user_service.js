import Vue from 'vue'

import {
  BEARER,
  BODY_TYPE_JSON
} from '@/services/constants'

/**
 * User Service
 * that request Player Content from server side
 */

export default {
  /**
   * Get server side users for CMS
   */
  getAllUsers: function (token) {
    return Vue.http.get(`./api/users`, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Add new user for CMS
   */
  addUser: function (token, user) {
    return Vue.http.post(`./api/users`, user, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Get user details by the username
   */
  getUserDetails: function (token, username) {
    return Vue.http.get(`./api/users/${username}`, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Edit user details by the username
   */
  editUserDetails: function (token, username, userDetails) {
    return Vue.http.patch(`./api/users/${username}`, userDetails, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Get user audit logs by the username
   */
  getUserAuditLogs: function (token, username, period) {
    return Vue.http.get(`./api/auditlogs/users/${username}?startTime=${period.startTime}&endTime=${period.endTime}`, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  }
}
