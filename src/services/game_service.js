import Vue from 'vue'

import {
  BEARER,
  BODY_TYPE_JSON,
  DEFAULT_LOCALE
} from '@/services/constants'

/**
 * Game Service
 * that request game content from server side
 */
export default {
  /**
   * Get game history by the game name
   */
  getGameHistory: function (token, gameName) {
    return Vue.http.get(`./api/gameHistory?gameName=${gameName}`, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Get All Games for CMS
   */
  getAllGames: function (token) {
    return Vue.http.get(`./api/game`, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Get category all games
   */
  getCategoryAllGames: function (token, category) {
    return Vue.http.get(`./api/game?category=${category}`, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Add new game
   */
  addGame: function (token, game) {
    return Vue.http.post(`./api/game`, game, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Get game play data by the game name
   *
   * @param {*} token
   * @param {*} game
   * @param {*} success
   * @param {*} error
   */
  getGame: function (token, game, locale = DEFAULT_LOCALE) {
    return Vue.http.get(`./api/game/${game}?loc=${locale}`, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Delete game by the game name
   */
  removeGame: function (token, gameName) {
    return Vue.http.delete(`./api/game/${gameName}`, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Get game details by the game name
   */
  getGameDetails: function (token, gameName) {
    return Vue.http.get(`./api/game/${gameName}/all`, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  },

  /**
   * Edit game details by the game name
   */
  editGameDetails: function (token, gameName, gameDetails) {
    return Vue.http.put(`./api/game/${gameName}`, gameDetails, {
      'headers': {
        'Accept': BODY_TYPE_JSON,
        'Authorization': `${BEARER} ${token}`
      }
    })
  }
}
