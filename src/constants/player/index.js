export const UPDATE_PLAYER = {
  STATUS_OPTIONS: [ 'Enabled', 'Disabled' ]
}

export const DISPLAY_PLAYER = {
  PLAYER_FIELDS: [
    { key: 'Username' },
    { key: 'Fullname' },
    { key: 'Email' },
    { key: 'Phone' },
    { key: 'Company' },
    { key: 'Status' },
    { key: 'Actions' } ],
  AVAILABLE_FIELDS: [
    'Username',
    'Fullname',
    'Email',
    'Phone',
    'Company',
    'Status',
    'Actions' ]
}

export const PLAYER_AUDIT_LOGS = {
  MAXIMUN_DAYS: 90 * 24 * 3600 * 1000, // 90 days
  DEFAULT_DAYS: 7 * 24 * 3600 * 1000, // 7 days
  AUDIT_LOGS_FIELDS: [
    { key: 'event' },
    { key: 'message' },
    { key: 'clientTime', label: 'Local Time' },
    { key: 'sourceIp' } ],
  AVAILABLE_FIELDS: [
    'event',
    'message',
    'clientTime',
    'sourceIp' ]
}
