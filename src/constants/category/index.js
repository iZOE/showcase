const CATEGORY = {
  DEFAULT: 'featured',
  LIVE_DEALER: 'livedealer',
  JACK_POT: 'jackpot'
}

export default CATEGORY
