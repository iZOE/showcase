export const USER_VALIDATION_PATTERN = {
  USERNAME: /^[a-zA-Z0-9_]+$/,
  FIRSTNAME: /^[a-zA-Z0-9_. -]+$/,
  LASTNAME: /^[a-zA-Z0-9_. -]+$/,
  PASSWORD: /^[a-zA-Z0-9_.!@]+$/,
  COMPANY: /^[a-zA-Z0-9_. -]+$/
}

export const USERNAME_ALL_IN_ONE_PATTERN = /^[a-zA-Z0-9_]{4,20}$/
