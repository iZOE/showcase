import Vue from 'vue'
import Router from 'vue-router'

import Lobby from '@/containers/Lobby.vue'
import LobbyHome from '@/containers/LobbyHome.vue'
import LobbyGame from '@/containers/LobbyGame.vue'
import LobbyLiveDealer from '@/containers/LobbyLiveDealer.vue'
import LobbyRegister from '@/containers/LobbyRegister.vue'
import LobbyDownload from '@/containers/LobbyDownload.vue'

import Logout from '@/containers/Logout.vue'
import PageNotFound from '@/containers/PageNotFound.vue'

import GamePlay from '@/components/gameplay/GamePlay.vue'

import Admin from '@/containers/Admin.vue'
import AdminNavigation from '@/components/admin/AdminNavigation.vue'
import AdminDisplayGame from '@/components/admin/game/AdminDisplayGame.vue'
import AdminCreateGame from '@/components/admin/game/AdminCreateGame.vue'
import AdminUpdateGame from '@/components/admin/game/AdminUpdateGame.vue'
import AdminDisplayBanner from '@/components/admin/banner/AdminDisplayBanner.vue'
import AdminCreateBanner from '@/components/admin/banner/AdminCreateBanner.vue'
import AdminUpdateBanner from '@/components/admin/banner/AdminUpdateBanner.vue'
import AdminDisplayPlayer from '@/components/admin/player/AdminDisplayPlayer.vue'
import AdminCreatePlayer from '@/components/admin/player/AdminCreatePlayer.vue'
import AdminUpdatePlayer from '@/components/admin/player/AdminUpdatePlayer.vue'
import AdminUpdatePlayerPassword from '@/components/admin/player/AdminUpdatePlayerPassword.vue'
import AdminDisplayPlayerAuditlogs from '@/components/admin/player/AdminDisplayPlayerAuditlogs.vue'

import store from '@/store'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      name: 'lobby',
      path: '/',
      component: Lobby,
      redirect: '/home',
      children: [
        {
          name: 'lobby-home',
          path: 'home',
          component: LobbyHome
        }, {
          name: 'lobby-game',
          path: 'game',
          component: LobbyGame
        }, {
          name: 'lobby-live-dealer',
          path: 'livedealer',
          component: LobbyLiveDealer
        }, {
          name: 'lobby-register',
          path: 'register',
          component: LobbyRegister
        }, {
          name: 'lobby-download',
          path: 'download',
          component: LobbyDownload,
          meta: { requiresAuth: true },
          beforeEnter: (to, from, next) => {
            // Validate user access again
            if (store.getters['auth/isLoggedIn']) {
              next()
            } else {
              next({ path: '/' })
            }
          }
        }
      ]
    }, {
      name: 'gameplay',
      path: '/gameplay/:gameName',
      component: GamePlay,
      meta: { requiresAuth: true },
      beforeEnter: (to, from, next) => {
        // Validate user access again
        if (store.getters['auth/isLoggedIn']) {
          next()
        } else {
          next({ path: '/' })
        }
      }
    }, {
      name: 'admin',
      path: '/admin',
      component: Admin,
      redirect: '/admin/game/display',
      meta: {
        requiresAuth: true,
        requiresAdmin: true
      },
      children: [
        {
          name: 'display',
          path: '/admin/game/display',
          component: AdminNavigation,
          children: [
            {
              name: 'admin-display-game',
              path: '/admin/game/display',
              component: AdminDisplayGame
            }, {
              name: 'admin-display-banner',
              path: '/admin/banner/display',
              component: AdminDisplayBanner
            }, {
              name: 'admin-display-player',
              path: '/admin/player/display',
              component: AdminDisplayPlayer
            }
          ]
        }, {
          name: 'admin-create-game',
          path: '/admin/game/create',
          component: AdminCreateGame
        }, {
          name: 'admin-update-game',
          path: '/admin/game/update/:gameName',
          component: AdminUpdateGame
        }, {
          name: 'admin-create-banner',
          path: '/admin/banner/create',
          component: AdminCreateBanner
        }, {
          name: 'admin-update-banner',
          path: '/admin/banner/update/:bannerId',
          component: AdminUpdateBanner
        }, {
          name: 'admin-create-player',
          path: '/admin/player/create',
          component: AdminCreatePlayer
        }, {
          name: 'admin-update-player',
          path: '/admin/player/update/:userName',
          component: AdminUpdatePlayer,
          props: true
        }, {
          name: 'admin-update-player-password',
          path: '/admin/player/update/:userName/resetPassword',
          component: AdminUpdatePlayerPassword,
          props: true
        }, {
          name: 'admin-display-player-auditlogs',
          path: '/admin/player/auditlogs/:userName',
          component: AdminDisplayPlayerAuditlogs,
          props: true
        }
      ]
    }, {
      name: 'logout',
      path: '/logout',
      component: Logout
    }, {
      name: 'page-not-found',
      path: '/404',
      component: PageNotFound
    }, {
      name: 'unknown',
      path: '/*',
      redirect: '/404'
    }
  ],
  scrollBehavior (to, from) {
    return { x: 0, y: 0 }
  }
})

router.beforeEach(async function (to, from, next) {
  // Do not allow access `to` URL until user logins
  if (to.matched.some(function (record) { return record.meta.requiresAuth })) {
    if (store.getters['auth/isLoggedIn']) {
      // Validate if user expired
      if (await store.dispatch('auth/actionIsUserExpired')) {
        store.dispatch('auth/actionShowExpiredPrompt')
      } else {
        next()
      }
    } else {
      if (from.name === null) {
        // Prevent from URL direct access, redirect the site entry
        next({ path: '/' })
      } else {
        // Do not allow access and show the login prompt
        store.dispatch('auth/actionShowLoginPrompt')
        next(false)
      }
    }
  } else {
    // Make sure always to call next()
    next()
  }
})

router.beforeEach(function (to, from, next) {
  if (to.matched.some(record => record.meta.requiresAdmin)) {
    if (store.getters['auth/isAdmin']) next()
    else next('/')
  } else {
    next()
  }
})
export default router
