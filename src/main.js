import Vue from 'vue'

import BootstrapVue from 'bootstrap-vue'
import VueResource from 'vue-resource'
import Vuelidate from 'vuelidate'
import { sync } from 'vuex-router-sync'

import App from '@/App'
import router from '@/router'
import store from '@/store'
import { i18n } from '@/locale'
import LocaleMixin from '@/mixins/locale_mixin'
import UtilityMixin from '@/mixins/utility_mixin'
import UserAgentMixin from '@/mixins/user_agent_mixin'
import { ERROR_CODE } from '@/constants/error_code'

import 'font-awesome/css/font-awesome.min.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@/assets/css/main.scss'

sync(store, router)

Vue.config.productionTip = false
Vue.config.devtools = true

Vue.use(BootstrapVue)
Vue.use(VueResource)
Vue.use(Vuelidate)
Vue.mixin(LocaleMixin)
Vue.mixin(UtilityMixin)
Vue.mixin(UserAgentMixin)

const showcase = new Vue({
  el: '#app',
  router,
  store,
  i18n,
  template: '<App/>',
  components: { App },
  data: {
    file: null
  }
})

window.app = showcase

Vue.http.interceptors.push(async (request, next) => {
  const isAuthorized = request.headers.map.Authorization
  if (isAuthorized) {
    const isUserExpired = await showcase.$store.dispatch('auth/actionIsUserExpired')
    if (isUserExpired) {
      showcase.$store.dispatch('auth/actionShowExpiredPrompt')
      return next(request.respondWith({
        status: 401,
        statusText: 'Unauthorized',
        ReturnCode: ERROR_CODE.TOKEN_EXPIRED
      }))
    }
  }
  next()
})
