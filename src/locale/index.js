import Vue from 'vue'
import VueI18n from 'vue-i18n'
import browserLocale from 'browser-locale'

Vue.use(VueI18n)

/* List Available Locales and Targets */
const localeSets = ['en-US', 'zh-CN']
const localeTargets = ['site', 'games', 'download', 'contact', 'modal', '404', 'error']
const _ = require('lodash')

function getDefaultLanguage (locales) {
  let language = browserLocale()
  console.log('[Action-Locale] The browser locale is', language)
  if (localStorage.getItem('locale')) {
    language = localStorage.getItem('locale')
    console.log(`[Action-Locale] The locale has been to set to [${language}].`)
  } else {
    language = _.includes(localeSets, language) ? language : 'en-US'
    localStorage.setItem('locale', language)
  }

  return language
}

function buildLocaleMessages (locales, localeTargets) {
  let messages = {}

  locales.forEach((locale) => {
    let localeFile = {}

    localeTargets.forEach((localeTarget) => {
      let localeContent = require('./' + locale.toLowerCase() + '/' + localeTarget + '.json')
      localeFile = _.merge(localeFile, localeContent)
    })

    messages[locale] = localeFile
  })

  return messages
}

export const i18n = new VueI18n({
  locale: getDefaultLanguage(localeSets),
  messages: buildLocaleMessages(localeSets, localeTargets)
})
