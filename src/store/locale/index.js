import * as types from '@/store/mutations_type'

const state = {
  locale: localStorage.getItem('locale') || 'en-US'
}

const getters = {
  getLocale: (state) => {
    return state.locale
  },
  getLocaleName: (state) => {
    if (state.locale === 'zh-CN') {
      return '中文'
    }
    if (state.locale === 'en-US') {
      return 'English'
    }
  }
}

const actions = {
  /**
  * Set Locale
  */
  actionSetLocale: ({ commit, state }, payload) => {
    let locale = payload.locale
    console.log(`[Action-Locale] Set Locale to ${locale}`)
    commit(types.STATE_LOCALE, locale)
    localStorage.setItem('locale', locale)
  }
}

const mutations = {
  [types.STATE_LOCALE] (state, locale) {
    state.locale = locale
  }
}

/**
 * Locale Store Module
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
