import UserService from '@/services/user_service'

const state = {}

const getters = {}

const actions = {
  actionCmsGetAllUsers: async ({ dispatch, commit }, payload) => {
    console.log(`[Action-User] Get CMS All Users.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await UserService.getAllUsers(payload.token)
      console.log('[Action-User] Get CMS All Users Success!')
      return res.body.Data.Users
    } catch (err) {
      console.warn('[Action-User] Get CMS All Users Fail!')
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsAddUser: async ({ dispatch, commit }, payload) => {
    const username = payload.userDetails.username
    console.log(`[Action-User] Add CMS User [${username}].`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await UserService.addUser(payload.token, payload.userDetails)
      console.log(`[Action-User] Add CMS User [${username}] Success!`)
      return res.body.Data
    } catch (err) {
      console.warn(`[Action-User] Add CMS User [${username}] Fail!'`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsGetUserDetails: async ({ dispatch, commit }, payload) => {
    const username = payload.username
    console.log(`[Action-User] Get CMS User [${username}] Details.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await UserService.getUserDetails(payload.token, username)
      console.log(`[Action-User] Get CMS User [${username}] Details Success!`)
      return res.body.Data.User
    } catch (err) {
      console.warn(`[Action-User] Get CMS User [${username}] Details Fail!`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsEditUserDetails: async ({ dispatch, commit }, payload) => {
    const username = payload.userName
    console.log(`[Action-User] Update CMS User [${username}] Details.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await UserService.editUserDetails(payload.token, username, payload.userDetails)
      console.log(`[Action-User] Update CMS User [${username}] Details Success!`)
      return res.body.Data.User
    } catch (err) {
      console.warn(`[Action-User] Update CMS User [${username}] Details Fail!`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsEditUserPassword: async ({ dispatch, commit }, payload) => {
    const username = payload.userName
    console.log(`[Action-User] Update CMS User [${username}] Details.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await UserService.editUserDetails(payload.token, username, payload.password)
      console.log(`[Action-User] Update CMS User [${username}] Password Success!`)
      return res.body.Data.User
    } catch (err) {
      console.warn(`[Action-User] Update CMS User [${username}] Password Fail!`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsIsUsernameExist: async ({ commit }, payload) => {
    const username = payload.username
    console.log(`[Action-User] Get CMS User [${username}].`)

    try {
      await UserService.getUserDetails(payload.token, username)
      console.log(`[Action-User] Get CMS User [${username}] Details Success!`)
      return
    } catch (err) {
      console.warn(`[Action-User] Get CMS User [${username}] Details Fail!`)
      throw err
    }
  },

  actionCmsGetUserAuditLogs: async ({ dispatch, commit }, payload) => {
    const username = payload.userName
    console.log(`[Action-User] Get CMS User [${username}] Audit Logs.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await UserService.getUserAuditLogs(payload.token, username, payload.period)
      console.log(`[Action-User] Get CMS User [${username}] Audit Logs Success!`)
      return res.body.Data
    } catch (err) {
      console.warn(`[Action-User] Get CMS User [${username}] Audit Logs Fail!`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  }
}

const mutations = {}

/**
 * User Store Module
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
