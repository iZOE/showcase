import MessageService from '@/services/message_service'

const state = {}

const getters = {}

const actions = {
  /**
   * Send Message Action
   */
  actionSendMessage: async ({ dispatch, commit }, payload) => {
    console.log('[Action-Message] Send Message')

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      let res = await MessageService.sendMessage(payload.message)
      console.log('[Action-Message] Send Message Success!')
      return res
    } catch (err) {
      console.log('[Action-Message] Send Message Fail!')
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  }
}

const mutations = {}

/**
 * Message Store Module
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
