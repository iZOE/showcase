import * as types from '@/store/mutations_type'
import actions from '@/store/category/actions'

const state = {}

const getters = {
  getCategories: (state) => {
    return state.categories
  }
}

const mutations = {
  [types.STATE_REQUEST_CATEGORIES] (state, categories) {
    state.categories = categories
  },
  [types.STATE_CLEAR_CATEGORIES] (state) {
    state.categories = undefined
  }
}

/**
 * Category Store Module
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
