import * as types from '@/store/mutations_type'
import CATEGORY from '@/constants/category'

import CategoryService from '@/services/category_service'

export default {
  /**
   * Get Category or Default Category
   */
  actionGetCategory: ({ commit }, category) => {
    return (category == null) ? CATEGORY.DEFAULT : category
  },

  /**
   * Check if input is LiveDealer Category
   */
  actionIsLiveDealer: ({ commit }, category) => {
    return category === CATEGORY.LIVE_DEALER
  },

  /**
   * Check if input is JackPot Category
   */
  actionIsJackPot: ({ commit }, category) => {
    return category === CATEGORY.JACK_POT
  },
  /**
 * Request Categories from server side API
  */
  actionGetCategories: async ({ dispatch, commit, state }, payload) => {
    console.log('[Action-Category] Get Categories')
    if (state.categories != null) {
      console.log('[Action-Category] Categories Exist')
      return state.categories
    }

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      console.log('[Action-Category] Send Request for Categories')
      const res = await CategoryService.getCategories(payload.locale)
      let categories = res.body.Data.Categories

      let sortCategories = categories.map((category) => {
        category.Name = category.Name.toLowerCase()
        return category
      })

      console.log(`[Action-Category] Get Categories Success`)
      commit(types.STATE_REQUEST_CATEGORIES, sortCategories)
      return sortCategories
    } catch (err) {
      console.log('[Action-Category] Get Categories Fail')
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  /**
   * Clear Category Games Cache
   */
  actionClearCategories: ({ commit }) => {
    console.log('[Action-Category] Clear Categories')
    commit(types.STATE_CLEAR_CATEGORIES)
  },

  /**
   * Request CMS Categories from server side API
   */
  actionCmsGetAllCategories: async ({ commit }) => {
    console.log(`[Action-Category] Request CMS All Categories.`)

    try {
      const res = await CategoryService.getCategories()
      console.log(`[Action-Category] Get CMS All Categories Success!`)
      return res.body.Data.Categories
    } catch (err) {
      console.warn(`[Action-Category] Get CMS All Categories Fail!`)
      throw err
    }
  },

  /**
   * Sort CMS Category Games from server side API
   */
  actionCmsSortCategoryGames: async ({ commit }, payload) => {
    const category = payload.category
    console.log(`[Action-Category] Sort CMS [${category}] Games.`)

    try {
      const res = await CategoryService.sortCategoryGames(payload.token, category, payload.allSortedGames)
      console.log(`[Action-Category] Sort CMS [${category}] Games Success!`)
      return res
    } catch (err) {
      console.warn(`[Action-Category] Sort CMS [${category}] Games Fail!'`)
      throw err
    }
  }
}
