import Vue from 'vue'
import Vuex from 'vuex'

import shared from '@/store/shared'
import banner from '@/store/banner'
import category from '@/store/category'
import message from '@/store/message'
import game from '@/store/game'
import auth from '@/store/auth'
import locale from '@/store/locale'
import user from '@/store/user'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    shared,
    banner,
    category,
    message,
    game,
    auth,
    locale,
    user
  }
})

export default store
