export const USER_ACCESS = 'showcase.access'
export const ACCESS_ACCOUNT = 'Account'
export const ACCESS_TOKEN = 'Token'
export const ACCESS_EXPIRATION = 'Expiration'
export const ACCESS_ROLES = 'Roles'
export const ACCESS_MG_USER = 'MgUser'
// 4 hours is token expiration, reduce 10 sec to prevent late expired.
export const ACCESS_VALID_TIMER = 14390000
export const CLAIM_TYPE = 'claimType'
export const CLAIM_VALUE = 'claimValue'
export const ROLE_ADMIN = 'Admin'
export const ROLE_PLAYER = 'Player'
