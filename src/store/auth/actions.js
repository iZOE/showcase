import * as types from '@/store/mutations_type'
import * as authConst from '@/store/auth/constants'
import AuthService from '@/services/auth_service'

export default {
  /**
   * Request Login Action
   */
  actionLogin: async ({ dispatch, commit }, payload) => {
    console.log('[Action-Auth] Request Login')
    commit(types.STATE_REQUEST_LOGIN)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      let res = await AuthService.login(payload.user)
      let userAccess = res.body.Data
      console.log('[Action-Auth] Login Success')
      commit(types.STATE_LOGIN_SUCCESS)

      userAccess[authConst.ACCESS_EXPIRATION] = Date.now() + authConst.ACCESS_VALID_TIMER

      localStorage.setItem(authConst.USER_ACCESS, JSON.stringify(userAccess))
      commit(types.STATE_SET_USER_ACCESS, userAccess)
    } catch (err) {
      console.log('[Action-Auth] Login Failed')
      commit(types.STATE_LOGIN_FAILED)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  /**
   * Request Logout Action
   */
  actionLogout: ({ commit }) => {
    console.log('[Action-Auth] Request Logout')
    commit(types.STATE_REQUEST_LOGOUT)
    localStorage.removeItem(authConst.USER_ACCESS)
  },

  /**
   * Check if user expired
   */
  actionIsUserExpired: ({ commit, state }) => {
    console.log('[Action-Auth] Check User Expires')
    let userAccess = state.userAccess || JSON.parse(localStorage.getItem(authConst.USER_ACCESS))

    if (userAccess == null) return true

    commit(types.STATE_SET_USER_ACCESS, userAccess)

    let expiration = userAccess[authConst.ACCESS_EXPIRATION]
    return !expiration || (Date.now() > parseInt(expiration))
  },

  /**
   * Show Login Prompt Before Entering Game Play Page
   */
  actionShowLoginPrompt: ({ commit }) => {
    console.log('[Action-Auth] Show Login Prompt Before Game Play')
    commit(types.STATE_SHOW_LOGIN_PROMPT)
  },

  /**
   * Hide Login Prompt Before Entering Game Play Page
   */
  actionHideLoginPrompt: ({ commit }) => {
    console.log('[Action-Auth] Hide Login Prompt Before Game Play')
    commit(types.STATE_HIDE_LOGIN_PROMPT)
  },

  /**
   * Show Expired Prompt
   */
  actionShowExpiredPrompt: ({ dispatch, commit }) => {
    console.log('[Action-Auth] Show Expired Prompt.')
    commit(types.STATE_SHOW_EXPIRED_PROMPT)
  },

  /**
   * Show Expired Prompt
   */
  actionHideExpiredPrompt: ({ commit }) => {
    console.log('[Action-Auth] Hide Expired Prompt.')
    commit(types.STATE_HIDE_EXPIRED_PROMPT)
  }
}
