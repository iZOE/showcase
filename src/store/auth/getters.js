import * as authConst from '@/store/auth/constants'

export default {
  /* Login Getter Group */
  isLoggedIn: (state) => {
    return state.isLoggedIn
  },
  isLoginDisabled: (state) => {
    return state.loginPending
  },
  isShownLoginPrompt: (state) => {
    return state.isShownLoginPrompt
  },
  isShownExpiredPrompt: (state) => {
    return state.isShownExpiredPrompt
  },

  /* Authentication Getter Group */
  userAccess: (state) => {
    return state.userAccess
  },
  userToken: (state) => {
    return (state.userAccess == null) ? '' : state.userAccess[authConst.ACCESS_TOKEN]
  },
  userRoles: (state) => {
    return (state.userAccess == null) ? [] : state.userAccess[authConst.ACCESS_ROLES]
  },
  userAccount: (state) => {
    return (state.userAccess == null) ? [] : state.userAccess[authConst.ACCESS_ACCOUNT]
  },
  mgUser: (state) => {
    return (state.userAccess == null) ? [] : state.userAccess[authConst.ACCESS_MG_USER]
  },
  liveAccount: (state) => {
    let isLiveAccountNull = (state.liveAccount.Account === null) && (state.liveAccount.Password === null)
    return isLiveAccountNull ? { 'Account': '', 'Password': '' } : state.liveAccount
  },
  isAdmin: (state) => {
    if ((state.userAccess == null)) return false
    if (state.userAccess[authConst.ACCESS_ROLES] == null) return false
    return state.userAccess[authConst.ACCESS_ROLES].indexOf(authConst.ROLE_ADMIN) > -1
  },
  isPlayer: (state) => {
    if ((state.userAccess == null)) return false
    if (state.userAccess[authConst.ACCESS_ROLES] == null) return false
    return state.userAccess[authConst.ACCESS_ROLES].indexOf(authConst.ROLE_PLAYER) > -1
  }
}
