import actions from '@/store/auth/actions'
import getters from '@/store/auth/getters'
import { state, mutations } from '@/store/auth/mutations'

/**
 * Auth Store Module
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
