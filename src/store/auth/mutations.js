import * as types from '@/store/mutations_type'
import * as authConst from '@/store/auth/constants'

export const state = {
  isLoggedIn: !!localStorage.getItem(authConst.USER_ACCESS),
  loginPending: false,
  isShownLoginPrompt: false,
  isShownExpiredPrompt: false,
  userAccess: JSON.parse(localStorage.getItem(authConst.USER_ACCESS)),
  liveAccount: undefined
}

export const mutations = {
  /* Login Mutation Group */
  [types.STATE_REQUEST_LOGIN] (state) {
    state.loginPending = true
  },
  [types.STATE_LOGIN_SUCCESS] (state) {
    state.isLoggedIn = true
    state.loginPending = false
  },
  [types.STATE_LOGIN_FAILED] (state) {
    state.loginPending = false
  },
  [types.STATE_REQUEST_LOGOUT] (state) {
    state.isLoggedIn = false
    state.loginPending = false
    state.userAccess = undefined
  },

  /* Authentication Mutation Group */
  [types.STATE_SET_USER_ACCESS] (state, userAccess) {
    state.userAccess = userAccess
    state.liveAccount = state.userAccess[authConst.ACCESS_MG_USER]
  },
  [types.STATE_SHOW_LOGIN_PROMPT] (state) {
    state.isShownLoginPrompt = true
  },
  [types.STATE_HIDE_LOGIN_PROMPT] (state) {
    state.isShownLoginPrompt = false
  },
  [types.STATE_SHOW_EXPIRED_PROMPT] (state) {
    state.isShownExpiredPrompt = true
  },
  [types.STATE_HIDE_EXPIRED_PROMPT] (state) {
    state.isShownExpiredPrompt = false
  }
}
