import * as types from '@/store/mutations_type'

const state = {
  isLoading: false
}

const getters = {
  isLoading: (state) => {
    return state.isLoading
  }
}

const actions = {
  actionShowLoading: ({ commit }) => {
    commit(types.STATE_SHOW_LOADING)
  },
  actionHideLoading: ({ commit }) => {
    commit(types.STATE_HIDE_LOADING)
  }
}

const mutations = {
  [types.STATE_SHOW_LOADING] (state) {
    state.isLoading = true
  },
  [types.STATE_HIDE_LOADING] (state) {
    state.isLoading = false
  }
}

/**
 * Shared Store Module
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
