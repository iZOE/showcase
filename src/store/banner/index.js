import * as types from '@/store/mutations_type'
import actions from '@/store/banner/actions'

const state = {
  banners: undefined
}

const getters = {
  getAllBanners: (state) => {
    return state.banners
  }
}

const mutations = {
  [types.STATE_SET_BANNERS] (state, banners) {
    state.banners = banners
  },
  [types.STATE_CLEAR_BANNERS] (state) {
    state.banners = undefined
  }
}

/**
 * Banner Store Module
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
