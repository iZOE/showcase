import * as types from '@/store/mutations_type'
import BannerService from '@/services/banner_service'
import ImageService from '@/services/image_service'

export default {
  actionGetBanners: async ({ dispatch, commit, state }, payload) => {
    const locale = payload.locale
    console.log(`[Action-Banner] Request Banners in [${locale}].`)
    if (state.banners !== undefined) {
      console.log(`[Action-Banner] Banners in [${locale}] exists.`)
      return state.banners
    }

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await BannerService.getBanners(locale)
      commit(types.STATE_SET_BANNERS, res.body.Data.Banners)
      console.log(`[Action-Banner] Get Banners in [${locale}] Success!`)
      return res.body.Data.Banners
    } catch (err) {
      console.warn(`[Action-Banner] Get Banners in [${locale}] Fail!`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsGetAllBanners: async ({ dispatch, commit }, payload) => {
    console.log(`[Action-Banner] Request CMS All Banners.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await BannerService.getAllBanners(payload.token)
      console.log('[Action-Banner] Get CMS All Banners Success!')
      return res.body.Data.Banners
    } catch (err) {
      console.warn('[Action-Banner] Get CMS Banners Fail!')
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsSortAllBanners: async ({ dispatch, commit }, payload) => {
    console.log(`[Action-Banner] Request Sort CMS All Banners.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await BannerService.sortAllBanners(payload.token, payload.allSortedBanners)
      console.log(`[Action-Banner] Sort CMS All Banners Success!`)
      return res
    } catch (err) {
      console.warn(`[Action-Banner] Sort CMS All Banners Fail!'`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsAddBanner: async ({ dispatch, commit }, payload) => {
    const bannerTitle = payload.banner.details[0].title
    console.log(`[Action-Banner] Request Add CMS Banner [${bannerTitle}].`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await BannerService.addBanner(payload.token, payload.banner)
      console.log(`[Action-Banner] Add CMS Banner [${bannerTitle}] Success!`)
      return res.body.Data
    } catch (err) {
      console.warn(`[Action-Banner] Add CMS Banner [${bannerTitle}] Fail!'`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsRemoveBanner: async ({ dispatch, commit }, payload) => {
    const bannerId = payload.bannerId
    console.log(`[Action-Banner] Remove CMS Banner [${bannerId}].`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await BannerService.removeBanner(payload.token, bannerId)
      console.log(`[Action-Banner] Remove CMS Banner [${bannerId}] Success!`)
      return res
    } catch (err) {
      console.warn(`[Action-Banner] Remove CMS  Banner [${bannerId}] Fail!'`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsGetBannerDetails: async ({ dispatch, commit }, payload) => {
    const bannerId = payload.bannerId
    console.log(`[Action-Banner] Request CMS Banner [${bannerId}] Details.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await BannerService.getBannerDetails(payload.token, bannerId)
      console.log(`[Action-Banner] Get CMS Banner [${bannerId}] Details Success!`)
      return res.body.Data.Banner
    } catch (err) {
      console.warn(`[Action-Banner] Get CMS Banner [${bannerId}] Details Fail!`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsEditBannerDetails: async ({ dispatch, commit }, payload) => {
    const bannerId = payload.bannerId
    console.log(`[Action-Banner] Request Edit CMS Banner [${bannerId}] Details.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await BannerService.editBannerDetails(payload.token, bannerId, payload.bannerDetails)
      console.log(`[Action-Banner] Update CMS Banner [${bannerId}] Details Success!`)
      return res.body.Data.Banner
    } catch (err) {
      console.warn(`[Action-Banner] Update CMS Banner [${bannerId}] Details Fail!`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsUploadBannerImage: async ({ dispatch, commit }, payload) => {
    console.log('[Action-Banner] Upload CMS Banner Image')
    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      let formData = new FormData()
      formData.append('type', 'banner')
      formData.append('file', payload.file, payload.file.name)
      const res = await ImageService.uploadImage(payload.token, formData)
      console.log('[Action-Banner] Upload CMS Banner Image Success')
      return res.body.Data
    } catch (err) {
      console.log('[Action-Banner] Upload CMS Banner Image Fail')
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionClearBanners: ({ commit }) => {
    console.log('[Action-Banner] Clear Banners')
    commit(types.STATE_CLEAR_BANNERS)
  }
}
