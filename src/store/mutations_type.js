/* Login Mutation Group */
export const STATE_REQUEST_LOGIN = 'login'
export const STATE_REQUEST_LOGOUT = 'logout'
export const STATE_LOGIN_SUCCESS = 'loginSuccess'
export const STATE_LOGIN_FAILED = 'loginFailed'

/* Authentication Mutation Group */
export const STATE_SET_USER_ACCESS = 'userAccess'
export const STATE_SHOW_LOGIN_PROMPT = 'showLoginPrompt'
export const STATE_HIDE_LOGIN_PROMPT = 'hideLoginPrompt'
export const STATE_SHOW_EXPIRED_PROMPT = 'showExpiredPrompt'
export const STATE_HIDE_EXPIRED_PROMPT = 'hideExpiredPrompt'

/* Category/Game Mutation Group */
export const STATE_REQUEST_CATEGORIES = 'requestCategories'
export const STATE_REQUEST_CATEGORY_GAMES = 'requestCategoryGames'
export const STATE_CLEAR_CATEGORIES = 'clearCategories'
export const STATE_CLEAR_GAMES = 'clearGames'

/* Banners */
export const STATE_SET_BANNERS = 'setBanners'
export const STATE_CLEAR_BANNERS = 'clearBanners'

/* Locale Mutation Group */
export const STATE_LOCALE = 'locale'

/* Shared Mutation Group */
export const STATE_SHOW_LOADING = 'showLoading'
export const STATE_HIDE_LOADING = 'hideLoading'
