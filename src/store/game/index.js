import * as types from '@/store/mutations_type'
import actions from '@/store/game/actions'

const state = {
  allCategoryGames: {}
}

const getters = {
  getAllCategoryGames: (state) => {
    return state.allCategoryGames
  }
}

const mutations = {
  [types.STATE_REQUEST_CATEGORY_GAMES] (state, data) {
    state.allCategoryGames[data.category] = data.games
  },
  [types.STATE_CLEAR_GAMES] (state) {
    state.allCategoryGames = {}
  }
}

/**
 * Game Store Module
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
