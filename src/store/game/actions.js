import * as types from '@/store/mutations_type'
import CategoryService from '@/services/category_service'
import GameService from '@/services/game_service'
import ImageService from '@/services/image_service'

export default {

  /**
   * Request Category Games from server side API
   */
  actionGetCategoryGames: async ({ dispatch, commit, state }, payload) => {
    let category = payload.category
    console.log(`[Action-Game] Get Category [${category}] Games`)
    if (state.allCategoryGames[category] != null) {
      console.log(`[Action-Game] Category Games [${category}] Exist`)
      return state.allCategoryGames[category]
    }

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      console.log(`[Action-Game] Send Request for Category [${category}] Games`)
      const res = await CategoryService.getCategoryGames(category, payload.locale, payload.isMobile)
      const games = res.body.Data.Games
      console.log(`[Action-Game] Get Category [${category}] Games Success`)
      commit(types.STATE_REQUEST_CATEGORY_GAMES, { category, games })
      return games
    } catch (err) {
      console.log(`[Action-Game] Get Category [${category}] Games Fail`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  /**
   * Clear Games
   */
  actionClearGames: ({ commit }) => {
    console.log('[Action-Game] Clear Games')
    commit(types.STATE_CLEAR_GAMES)
  },

  actionGetGame: async ({ dispatch, commit }, payload) => {
    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      console.log('[Action-Game] Get Game Data.')
      const res = await GameService.getGame(payload.token, payload.gameName, payload.locale)
      console.log('[Action-Game] Get Game Data Success.')
      return res.body.Data.Game
    } catch (err) {
      console.log('[Action-Game] Get Game Data Fail.')
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  /**
   * Get Game History
   */
  actionGetGameHistory: async ({ dispatch, commit }, payload) => {
    console.log('[Action-Game] Get Game History.')

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await GameService.getGameHistory(payload.token, payload.gamename)
      console.log('[Action-Game] Get Game History Success!')
      return res.body.Data
    } catch (err) {
      console.log('[Action-Game] Get Game History Fail!')
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsGetAllGames: async ({ dispatch, commit }, payload) => {
    console.log(`[Action-Game] Get CMS All Games.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await GameService.getAllGames(payload.token)
      console.log(`[Action-Game] Get CMS All Games Success!`)
      return res.body.Data.Games
    } catch (err) {
      console.warn(`[Action-Game] Get CMS All Games Fail!`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsGetCategoryGames: async ({ dispatch, commit }, payload) => {
    let category = payload.category
    console.log(`[Action-Game] Get CMS [${category}] Games.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await GameService.getCategoryAllGames(payload.token, category)
      console.log(`[Action-Game] Get CMS [${category}] Games Success!`)
      return res.body.Data.Games
    } catch (err) {
      console.warn(`[Action-Game] Get CMS [${category}] Games Fail!`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsAddGame: async ({ dispatch, commit }, payload) => {
    const gameName = payload.gameName
    console.log(`[Action-Game] Add CMS Game [${gameName}].`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await GameService.addGame(payload.token, payload.game)
      console.log(`[Action-Game] Add CMS Game [${gameName}] Success!`)
      return res.body.Data
    } catch (err) {
      console.warn(`[Action-Game] Add CMS Game [${gameName}] Fail!'`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsRemoveGame: async ({ dispatch, commit }, payload) => {
    const gameName = payload.gameName
    console.log(`[Action-Game] Remove CMS Game [${gameName}].`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await GameService.removeGame(payload.token, gameName)
      console.log(`[Action-Game] Remove CMS Game [${gameName}] Success!`)
      return res
    } catch (err) {
      console.warn(`[Action-Game] Remove CMS Game [${gameName}] Fail!'`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsIsGameNameExist: async ({ dispatch, commit }, payload) => {
    const gameName = payload.gameName
    console.log(`[Action-Game] Check CMS Game [${gameName}].`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      await GameService.getGame(payload.token, gameName)
      console.log(`[Action-Game] Get CMS Game [${gameName}] Success!`)
      return
    } catch (err) {
      console.warn(`[Action-Game] Get CMS Game [${gameName}] Fail!'`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsGetGameDetails: async ({ dispatch, commit }, payload) => {
    const gameName = payload.gameName
    console.log(`[Action-Game] Get CMS Game [${gameName}] Details.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await GameService.getGameDetails(payload.token, gameName)
      console.log(`[Action-Game] Get CMS Game [${gameName}] Details Success!`)
      return res.body.Data.Game
    } catch (err) {
      console.warn(`[Action-Game] Get CMS Game [${gameName}] Details Fail!`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsEditGameDetails: async ({ dispatch, commit }, payload) => {
    const gameName = payload.gameName
    console.log(`[Action-Game] Update CMS Game [${gameName}] Details.`)

    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      const res = await GameService.editGameDetails(payload.token, gameName, payload.gameDetails)
      console.log(`[Action-Game] Update CMS Game [${gameName}] Details Success!`)
      return res.body.Data.Game
    } catch (err) {
      console.warn(`[Action-Game] Update CMS Game [${gameName}] Details Fail!`)
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  },

  actionCmsUploadGameImage: async ({ dispatch, commit }, payload) => {
    console.log('[Action-Game] Upload CMS Game Image')
    try {
      dispatch('shared/actionShowLoading', {}, {root: true})
      let formData = new FormData()
      formData.append('file', payload.file, payload.file.name)
      const res = await ImageService.uploadImage(payload.token, formData)
      console.log('[Action-Game] Upload CMS Game Image Success.')
      return res.body.Data
    } catch (err) {
      console.log('[Action-Game] Upload CMS Game Image Fail.')
      throw err
    } finally {
      dispatch('shared/actionHideLoading', {}, {root: true})
    }
  }
}
